package centralUnit;

import alice.logictuple.LogicTuple;
import alice.logictuple.exceptions.InvalidLogicTupleException;
import alice.tucson.api.EnhancedSynchACC;
import alice.tucson.api.TucsonAgentId;
import alice.tucson.api.TucsonMetaACC;
import alice.tucson.api.TucsonTupleCentreId;
import alice.tucson.api.exceptions.TucsonInvalidAgentIdException;
import alice.tucson.api.exceptions.TucsonInvalidTupleCentreIdException;
import alice.tucson.api.exceptions.TucsonOperationNotPossibleException;
import alice.tucson.api.exceptions.UnreachableNodeException;
import alice.tuplecentre.api.exceptions.OperationTimeOutException;
import utils.Utils;
import com.pi4j.io.gpio.event.GpioPinDigitalStateChangeEvent;
import com.pi4j.io.gpio.event.GpioPinListenerDigital;

/**
 *
 */
public class ButtonListener implements GpioPinListenerDigital {
	private EnhancedSynchACC acc;
	private TucsonTupleCentreId buttonTc;

	public ButtonListener() {
		try {
			final TucsonAgentId aid = new TucsonAgentId("buttonListener");
			this.acc = TucsonMetaACC.getContext(aid, Utils.defaultIp, Integer.valueOf(Utils.defaultPort));
			this.buttonTc = new TucsonTupleCentreId("buttonTc", Utils.defaultIp, Utils.defaultPort);
		} catch (final TucsonInvalidTupleCentreIdException e) {
			e.printStackTrace();
		} catch (final TucsonInvalidAgentIdException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void handleGpioPinDigitalStateChangeEvent(GpioPinDigitalStateChangeEvent event) {
		// display pin state on console

		System.out.println(" --> GPIO PIN STATE CHANGE: " + event.getPin() + " = " + event.getState());
		try {
			if (event.getState().isLow()) {
				this.acc.out(buttonTc, LogicTuple.parse("button(1)"), null);
			}
		} catch (InvalidLogicTupleException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TucsonOperationNotPossibleException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnreachableNodeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (OperationTimeOutException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}

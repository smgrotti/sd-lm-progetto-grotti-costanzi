package centralUnit;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import alice.logictuple.LogicTuple;
import alice.logictuple.exceptions.InvalidLogicTupleException;
import alice.tucson.api.AbstractSpawnActivity;
import alice.tucson.api.exceptions.TucsonInvalidLogicTupleException;
import utils.Utils;

/**
 *
 *
 * @author s.mariani@unibo.it
 */
public class UpdateComunicationTheory extends AbstractSpawnActivity {

	private static final long serialVersionUID = -4459068799410719933L;

    @Override
    public void doActivity() {
        try {
        	final List<LogicTuple> res = new LinkedList<LogicTuple>();
        	res.add(this.rd(LogicTuple.parse("controllers_list(C)")));
        	res.add(this.rd(LogicTuple.parse("controlled_list(C)")));
            //put new tuples into specified file           
            Utils.stringToFile("bin/centralUnit/comunicationTheory.rsp", res.toString());
        } catch (final InvalidLogicTupleException e) {
            this.log("ERROR: Tuple is not an admissible Prolog term!");
            e.printStackTrace();
        } /*catch (final TucsonInvalidLogicTupleException e) {
            this.log("ERROR: Tuple is not an admissible Prolog term!");
            e.printStackTrace();
        }*/ catch (IOException e) {
        	this.log("ERROR: An I/O problem occurred when updating communication theory!");
			e.printStackTrace();
		}
    }
}

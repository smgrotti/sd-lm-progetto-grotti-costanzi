package centralUnit;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import com.pi4j.component.lcd.LCDTextAlignment;
import com.pi4j.component.lcd.impl.GpioLcdDisplay;
import com.pi4j.io.gpio.RaspiPin;
import alice.logictuple.LogicTuple;
import alice.logictuple.TupleArgument;
import alice.logictuple.Value;
import alice.logictuple.exceptions.InvalidLogicTupleException;
import alice.tucson.api.AbstractTucsonAgent;
import alice.tucson.api.EnhancedSynchACC;
import alice.tucson.api.ITucsonOperation;
import alice.tucson.api.NegotiationACC;
import alice.tucson.api.TucsonMetaACC;
import alice.tucson.api.TucsonTupleCentreId;
import alice.tucson.api.exceptions.TucsonInvalidAgentIdException;
import alice.tucson.api.exceptions.TucsonInvalidTupleCentreIdException;
import alice.tucson.api.exceptions.TucsonOperationNotPossibleException;
import alice.tucson.api.exceptions.UnreachableNodeException;
import alice.tucson.service.TucsonNodeService;
import alice.tuplecentre.api.exceptions.OperationTimeOutException;
import alice.tuplecentre.core.AbstractTupleCentreOperation;
import utils.Utils;

public class CentralUnit extends AbstractTucsonAgent {

	public static void main(final String[] args) {
		if (args.length == 0) {
			new TucsonNodeService().install();
		} else if (args.length == 1) {
			new TucsonNodeService(Integer.parseInt(args[0])).install();
		} else {
			System.out.println("USAGE: java TucsonNodeLauncher [portno]");
		}
		try {
			new CentralUnit("centralUnit", Utils.serverTcName + "@" + 
					Utils.defaultIp + ":" + Utils.defaultPort).go();
		} catch (final TucsonInvalidAgentIdException e) {
			e.printStackTrace();
		}
	}

	private EnhancedSynchACC acc;
	private TucsonTupleCentreId serverTc;
	private TucsonTupleCentreId configTc;
	private TucsonTupleCentreId sensorTc;
	private TucsonTupleCentreId actuatorTc;
	private LogicTuple tuple;
	private ITucsonOperation operation;
	private String childrenNameOutside;
	private SimpleDateFormat formatter;
	private GpioLcdDisplay lcd;
	private final static int LCD_ROWS = 2;
	private final static int LCD_COLUMNS = 16;
	private final static int LCD_ROW_1 = 0;
	private final static int LCD_ROW_2 = 1;

	/**
	 * Creation of all tuples centers.
	 * 
	 * @param name
	 *            Name of the agent CentralUnit
	 * @param tcID
	 *            Name of the central unit tuple centre
	 * @throws TucsonInvalidAgentIdException
	 *             If the chosen ID is not a valid TuCSoN agent ID
	 */
	public CentralUnit(String name, String tcID) throws TucsonInvalidAgentIdException {
		super(name);
		formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			this.serverTc = new TucsonTupleCentreId(tcID);
			this.configTc = new TucsonTupleCentreId("'$ENV'", Utils.defaultIp, Utils.defaultPort);
			this.sensorTc = new TucsonTupleCentreId("sensorTc", Utils.defaultIp, Utils.defaultPort);
			this.actuatorTc = new TucsonTupleCentreId("actuatorTc", Utils.defaultIp, Utils.defaultPort);
		} catch (final TucsonInvalidTupleCentreIdException e) {
			this.say("Invalid tuple centre id given, killing myself...");
		}
	}

	@Override
	protected void main() {
		this.say("I'm started.");
		try {
			init();
			/* Start loop: -wait alert -turn on the led actuator -sense on the
			   button sensor -turn off the led */
			this.say("Start loop: -wait alert -act on the led actuator -sense on the button sensor -act on the led ...");
			while (true) {
				waitAlert();
				alarmActivation();
				senseButton();
				alarmDeactivation();
			}
		} catch (final TucsonOperationNotPossibleException e) {
			this.say("ERROR: Never seen this happen before *_*");
		} catch (final UnreachableNodeException e) {
			this.say("ERROR: Given TuCSoN Node is unreachable!");
			e.printStackTrace();
		} catch (final OperationTimeOutException e) {
			this.say("ERROR: Endless timeout expired!");
		} catch (final TucsonInvalidAgentIdException e) {
			this.say("ERROR: Given ID is not a valid TuCSoN agent ID!");
		} catch (IOException e) {
			this.say("ERROR: IO problem with the ReSpecT specification file!");
			e.printStackTrace();
		} catch (InvalidLogicTupleException e) {
			this.say("ERROR: The tuple created is invalid!");
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (acc != null) {
				try {
					acc.exit();
				} catch (final TucsonOperationNotPossibleException e) {
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	public void operationCompleted(AbstractTupleCentreOperation arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void operationCompleted(ITucsonOperation arg0) {
		// TODO Auto-generated method stub

	}

	/**
	 * Initial configuration.
	 * @throws InterruptedException 
	 */
	public void init() throws TucsonOperationNotPossibleException, UnreachableNodeException, OperationTimeOutException,
			TucsonInvalidAgentIdException, InvalidLogicTupleException, IOException, InterruptedException {
		createAgentContext();
		setUpTheories();
		setUpButtonSensor();
		setUpLedActuator();
		setUpLCD();
	}

	/**
	 * Creation of the agent context.
	 */
	public void createAgentContext() throws TucsonOperationNotPossibleException, UnreachableNodeException,
			OperationTimeOutException, TucsonInvalidAgentIdException {
		final NegotiationACC negAcc = TucsonMetaACC.getNegotiationContext(this.getTucsonAgentId());
		this.acc = negAcc.playDefaultRole();
	}

	/**
	 * Import the ordinary and specification tuples from file and injecting
	 * these into the tuple centre of the centrl unit.
	 */
	public void setUpTheories() throws IOException, InvalidLogicTupleException, TucsonOperationNotPossibleException,
			UnreachableNodeException, OperationTimeOutException {
		/* import ordinary and specification tuples from file */
		String comunicationTheory = Utils.fileToString("centralUnit/comunicationTheory.rsp");
		String coordinationTheory = Utils.fileToString("centralUnit/coordinationTheory.rsp");
		/* "Injecting coordination and comunication theory" */
		this.say("Injecting coordination and comunication theory " + "in tc < " + serverTc.toString() + " >...");
		acc.setS(serverTc, coordinationTheory, null);
		acc.set(serverTc, LogicTuple.parse(comunicationTheory), null);
		/* Alternative injecting comunication theory */
		/*
		 * acc.out(tid, LogicTuple.parse("controllers_list([])"), null);
		 * acc.out(tid, LogicTuple.parse("controlled_list([])"), null);
		 */
	}

	/**
	 * Set up the button sensor and its transducer and finally injecting the
	 * specification tuple into sensor tuple centre.
	 */
	public void setUpButtonSensor() throws TucsonOperationNotPossibleException, UnreachableNodeException,
			OperationTimeOutException, IOException {
		this.say("Set up sensor...");
		acc.setS(sensorTc, Utils.fileToString("centralUnit/sensorSpec.rsp"), null);
		final LogicTuple sensorTuple = new LogicTuple("createTransducerSensor", new TupleArgument(sensorTc.toTerm()),
				new Value("centralUnit.SensorTransducer"), new Value("sensorTransducer"),
				new Value("centralUnit.ButtonSensor"), new Value("sensor"));
		acc.out(configTc, sensorTuple, null);
	}

	/**
	 * Set up the led actuator and its transducer and finally injecting the
	 * specification tuple into actuator tuple centre.
	 */
	public void setUpLedActuator() throws TucsonOperationNotPossibleException, UnreachableNodeException,
			OperationTimeOutException, IOException {
		this.say("Set up actuator...");
		acc.setS(actuatorTc, Utils.fileToString("centralUnit/actuatorSpec.rsp"), null);
		final LogicTuple actuatorTuple = new LogicTuple("createTransducerActuator",
				new TupleArgument(actuatorTc.toTerm()), new Value("centralUnit.ActuatorTransducer"),
				new Value("actuatorTransducer"), new Value("centralUnit.LedActuator"), new Value("actuator"));
		acc.out(configTc, actuatorTuple, null);
	}

	/**
	 * @throws InterruptedException 
	 * 
	 */
	public void setUpLCD() throws InterruptedException{
		lcd = new GpioLcdDisplay(LCD_ROWS,          // number of row supported by LCD
                LCD_COLUMNS,       // number of columns supported by LCD
                RaspiPin.GPIO_11,  // LCD RS pin
                RaspiPin.GPIO_10,  // LCD strobe pin
                RaspiPin.GPIO_00,  // LCD data bit 1
                RaspiPin.GPIO_04,  // LCD data bit 2
                RaspiPin.GPIO_05,  // LCD data bit 3
                RaspiPin.GPIO_06); // LCD data bit 4
        lcd.clear();
        //Thread.sleep(1000);
        lcd.writeln(LCD_ROW_1, formatter.format(new Date()), LCDTextAlignment.ALIGN_CENTER);
	}
	
	/**
	 * The central unit wait for an alert of a child who go outside of the area.
	 */
	public void waitAlert() throws InvalidLogicTupleException, TucsonOperationNotPossibleException,
			UnreachableNodeException, OperationTimeOutException {
		tuple = LogicTuple.parse("internalAlert(Name)");
		operation = acc.in(serverTc, tuple, null);
		if (operation.isResultSuccess()) {
			tuple = operation.getLogicTupleResult();
			childrenNameOutside = (""+tuple.getArg(0)).toUpperCase();
			this.say("The children " + childrenNameOutside + " is outside the area");
		}
	}

	/**
	 * Just received the alert turn on the LED to create alarm.
	 */
	public void alarmActivation() throws InvalidLogicTupleException, TucsonOperationNotPossibleException,
			UnreachableNodeException, OperationTimeOutException {
		tuple = LogicTuple.parse("act(light(1))");
		acc.out(actuatorTc, tuple, null);
		 //write to lcd the person who is outside
		lcd.clear();
		lcd.writeln(LCD_ROW_1, "!!! ALARM !!!", LCDTextAlignment.ALIGN_CENTER);
		lcd.writeln(LCD_ROW_2, childrenNameOutside, LCDTextAlignment.ALIGN_CENTER);
	}

	/**
	 * Sense the button sensor to know when disconnect alarm.
	 */
	public void senseButton() throws InvalidLogicTupleException, TucsonOperationNotPossibleException,
			UnreachableNodeException, OperationTimeOutException {
		tuple = LogicTuple.parse("sense(button(1))");
		acc.in(sensorTc, tuple, null);
	}

	/**
	 * Just perceived that the button is pressed, the LED is turned off.
	 */
	public void alarmDeactivation() throws TucsonOperationNotPossibleException, UnreachableNodeException,
			OperationTimeOutException, InvalidLogicTupleException {
		tuple = LogicTuple.parse("act(light(0))");
		acc.out(actuatorTc, tuple, null);
		//write to lcd the date of today
		lcd.clear();
	    lcd.writeln(LCD_ROW_1, formatter.format(new Date()), LCDTextAlignment.ALIGN_CENTER);
	}
}
package centralUnit;

import alice.logictuple.LogicTuple;
import alice.logictuple.exceptions.InvalidLogicTupleException;
import alice.respect.core.TransducersManager;
import alice.respect.situatedness.AbstractProbeId;
import alice.respect.situatedness.AbstractTransducer;
import alice.respect.situatedness.ISimpleProbe;
import alice.respect.situatedness.TransducerId;
import alice.respect.situatedness.TransducerStandardInterface;
import alice.tucson.api.EnhancedSynchACC;
import alice.tucson.api.ITucsonOperation;
import alice.tucson.api.TucsonAgentId;
import alice.tucson.api.TucsonMetaACC;
import alice.tucson.api.TucsonTupleCentreId;
import alice.tucson.api.exceptions.TucsonInvalidAgentIdException;
import alice.tucson.api.exceptions.TucsonInvalidTupleCentreIdException;
import alice.tucson.api.exceptions.TucsonOperationNotPossibleException;
import alice.tucson.api.exceptions.UnreachableNodeException;
import alice.tuplecentre.api.exceptions.InvalidOperationException;
import alice.tuplecentre.api.exceptions.OperationTimeOutException;
import utils.Utils;
import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.PinPullResistance;
import com.pi4j.io.gpio.RaspiPin;


/**
 *
 */
public class ButtonSensor implements ISimpleProbe {

	private EnhancedSynchACC acc;
	private final AbstractProbeId pid;
	private TucsonTupleCentreId buttonTc;
	private TransducerId tid;
	private TransducerStandardInterface transducer;

	final private GpioController gpio;
	final GpioPinDigitalInput myButton;

	public ButtonSensor(final AbstractProbeId i) {
		this.pid = i;
		// create gpio controller
		this.gpio = GpioFactory.getInstance();
		// provision gpio pin #02 as an input pin with its internal pull down
		// resistor enabled
		this.myButton = gpio.provisionDigitalInputPin(RaspiPin.GPIO_02, PinPullResistance.PULL_DOWN);
		// create and register gpio pin listener
		ButtonListener buttonListener = new ButtonListener();
		myButton.addListener(buttonListener);

		try {
			final TucsonAgentId aid = new TucsonAgentId("sensor");
			this.acc = TucsonMetaACC.getContext(aid, Utils.defaultIp, Integer.valueOf(Utils.defaultPort));
			this.buttonTc = new TucsonTupleCentreId("buttonTc", Utils.defaultIp, Utils.defaultPort);
		} catch (final TucsonInvalidTupleCentreIdException e) {
			e.printStackTrace();
		} catch (final TucsonInvalidAgentIdException e) {
			e.printStackTrace();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see alice.respect.situatedness.ISimpleProbe#getIdentifier()
	 */
	@Override
	public AbstractProbeId getIdentifier() {
		return this.pid;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see alice.respect.situatedness.ISimpleProbe#getTransducer()
	 */
	@Override
	public TransducerId getTransducer() {
		return this.tid;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see alice.respect.situatedness.ISimpleProbe#readValue(java.lang.String)
	 */
	@Override
	public boolean readValue(final String key) {
		if (!"button".equals(key)) {
			System.err.println("[" + this.pid + "]: Unknown property " + key);
			return false;
		}
		if (this.tid == null) {
			System.err.println("[" + this.pid + "]: Don't have any transducer associated yet!");
			return false;
		}
		if (this.transducer == null) {
			this.transducer = TransducersManager.INSTANCE.getTransducer(this.tid.getAgentName());
			if (this.transducer == null) {
				System.err.println("[" + this.pid + "]: Can't retrieve my transducer!");
				return false;
			}
		}
		try {
			final LogicTuple tupleButtonPressed = LogicTuple.parse("button(1)");
			final LogicTuple tupleClean = LogicTuple.parse("button(V)");
			this.acc.inAll(buttonTc, tupleClean, null);
			final ITucsonOperation op = this.acc.in(this.buttonTc, tupleButtonPressed, null);
			if (op.isResultSuccess()) {
				this.transducer.notifyEnvEvent(key, 1, AbstractTransducer.GET_MODE);
			}
			return true;
		} catch (final TucsonOperationNotPossibleException e) {
			e.printStackTrace();
		} catch (final UnreachableNodeException e) {
			e.printStackTrace();
		} catch (final OperationTimeOutException e) {
			e.printStackTrace();
		} catch (final InvalidOperationException e) {
			e.printStackTrace();
		} catch (final InvalidLogicTupleException e) {
			e.printStackTrace();
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see alice.respect.situatedness.ISimpleProbe#setTransducer(alice.respect.
	 * situatedness.TransducerId)
	 */
	@Override
	public void setTransducer(final TransducerId t) {
		this.tid = t;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see alice.respect.situatedness.ISimpleProbe#writeValue(java.lang.String,
	 * int)
	 */
	@Override
	public boolean writeValue(final String key, final int value) {
		System.err.println("[" + this.pid + "]: I'm a sensor, I can't set values!");
		return false;
	}
}

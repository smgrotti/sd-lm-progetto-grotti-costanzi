/**
          * ActualActuator.java
 */
package centralUnit;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.RaspiPin;

import alice.logictuple.LogicTuple;
import alice.logictuple.exceptions.InvalidLogicTupleException;
import alice.respect.core.TransducersManager;
import alice.respect.situatedness.AbstractProbeId;
import alice.respect.situatedness.AbstractTransducer;
import alice.respect.situatedness.ISimpleProbe;
import alice.respect.situatedness.TransducerId;
import alice.respect.situatedness.TransducerStandardInterface;
import alice.tucson.api.EnhancedSynchACC;
import alice.tucson.api.TucsonAgentId;
import alice.tucson.api.TucsonMetaACC;
import alice.tucson.api.TucsonTupleCentreId;
import alice.tucson.api.exceptions.TucsonInvalidAgentIdException;
import alice.tucson.api.exceptions.TucsonInvalidTupleCentreIdException;
import alice.tucson.api.exceptions.TucsonOperationNotPossibleException;
import alice.tucson.api.exceptions.UnreachableNodeException;
import alice.tuplecentre.api.exceptions.OperationTimeOutException;
import utils.Utils;

/**
 * The 'actual' actuator probe deployed in this scenario. Although in this toy
 * example it is only simulated, here is where you would place your code to
 * interface with a real-world probe.
 *
 * @author ste (mailto: s.mariani@unibo.it) on 06/nov/2013
 *
 */
public class LedActuator implements ISimpleProbe {
	
	private EnhancedSynchACC acc;
    private TucsonTupleCentreId buttonTc;
    private final AbstractProbeId pid;
    private TransducerId tid;
    private TransducerStandardInterface transducer;
    
    private final GpioController gpio;
    private final GpioPinDigitalOutput pin;

    public LedActuator(final AbstractProbeId i) {
    	this.pid = i;
    	// create gpio controller
        this.gpio = GpioFactory.getInstance();
        // provision gpio pin #01 as an output pin and turn on
        this.pin = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_01, "MyLED");
        pin.low();
    	try {
    		final TucsonAgentId aid = new TucsonAgentId("actuator");
    		this.acc = TucsonMetaACC.getContext(aid, Utils.defaultIp,
    				Integer.valueOf(Utils.defaultPort));
    		this.buttonTc = new TucsonTupleCentreId("buttonTc",
    				Utils.defaultIp, Utils.defaultPort);
	    } catch (final TucsonInvalidTupleCentreIdException e) {
	        e.printStackTrace();
	    } catch (final TucsonInvalidAgentIdException e) {
	    	e.printStackTrace();
    	}
    }

    /*
     * (non-Javadoc)
     * @see alice.respect.situatedness.ISimpleProbe#getIdentifier()
     */
    @Override
    public AbstractProbeId getIdentifier() {
        return this.pid;
    }

    /*
     * (non-Javadoc)
     * @see alice.respect.situatedness.ISimpleProbe#getTransducer()
     */
    @Override
    public TransducerId getTransducer() {
        return this.tid;
    }

    /*
     * (non-Javadoc)
     * @see alice.respect.situatedness.ISimpleProbe#readValue(java.lang.String)
     */
    @Override
    public boolean readValue(final String key) {
        System.err.println("[" + this.pid
                + "]: I'm an actuator, I can't sense values!");
        return false;
    }

    /*
     * (non-Javadoc)
     * @see alice.respect.situatedness.ISimpleProbe#setTransducer(alice.respect.
     * situatedness.TransducerId)
     */
    @Override
    public void setTransducer(final TransducerId t) {
        this.tid = t;
    }

    /*
     * (non-Javadoc)
     * @see alice.respect.situatedness.ISimpleProbe#writeValue(java.lang.String,
     * int)
     */
    @Override
    public boolean writeValue(final String key, final int value) {
        if (!"light".equals(key)) {
            System.err.println("[" + this.pid + "]: Unknown property " + key);
            return false;
        }
        if (this.tid == null) {
            System.err.println("[" + this.pid
                    + "]: Don't have any transducer associated yet!");
            return false;
        }
        if (this.transducer == null) {
            this.transducer = TransducersManager.INSTANCE
                    .getTransducer(this.tid.getAgentName());
            if (this.transducer == null) {
                System.err.println("[" + this.pid
                        + "]: Can't retrieve my transducer!");
                return false;
            }
        }
        try {
        	/* Qui lui agisce su una proprioetà dell'ambiente che è la temperatura in modo
        	 * simulato quindi ha dovuto crearsi lui sta proprioetà con un centro di tuple dove
        	 * nella tupla cè il vaore di temperatura 
        	 * Ma noi non ne abbiamo bisogno di simulare niente perchè ho proprio il led...*/
            
        	/*final LogicTuple template = LogicTuple.parse("temp(_)");
            final ITucsonOperation op = this.acc.inAll(this.tempTc, template,
                    null);
            if (op.isResultSuccess()) {
                final LogicTuple tempTuple = LogicTuple.parse("temp(" + value
                        + ")");
                this.acc.out(this.tempTc, tempTuple, null);*/
        	
            System.out.println("[" + this.pid + "]: led set to " + value);
            this.acc.out(this.buttonTc, LogicTuple.parse("light("+ value +")"), null);
            
            /* Management the LED */
            if(value == 1){
            	pin.high();
            }else{
            	pin.low();
            }
            
            this.transducer.notifyEnvEvent(key, value,
                    AbstractTransducer.SET_MODE);
            return true;

        } catch (final TucsonOperationNotPossibleException e) {
            e.printStackTrace();
        } catch (final UnreachableNodeException e) {
            e.printStackTrace();
        } catch (final OperationTimeOutException e) {
            e.printStackTrace();
        } catch (InvalidLogicTupleException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return false;
    }
}

package centralUnit;

import alice.logictuple.LogicTuple;
import alice.logictuple.exceptions.InvalidLogicTupleException;
import alice.tucson.api.AbstractSpawnActivity;
import alice.tucson.api.exceptions.TucsonInvalidLogicTupleException;

/**
 *
 *
 * @author s.mariani@unibo.it
 */
public class ButtonPressedSimulated extends AbstractSpawnActivity {

	private static final long serialVersionUID = -4459068799410719933L;

    @Override
    public void doActivity() {
        try {
        	Thread.sleep(60000);
        	this.out(LogicTuple.parse("button(1)"));
        } catch (final InvalidLogicTupleException e) {
            this.log("ERROR: Tuple is not an admissible Prolog term!");
            e.printStackTrace();
        } /*catch (final TucsonInvalidLogicTupleException e) {
            this.log("ERROR: Tuple is not an admissible Prolog term!");
            e.printStackTrace();
        }*/ catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
}

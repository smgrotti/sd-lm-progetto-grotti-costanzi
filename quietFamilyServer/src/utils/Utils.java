package utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class Utils {
	
	public static final String defaultIp = "localhost";
	public static final String defaultPort = "20504";
	public static final String serverTcName = "coordinationTC";
	
	public static String fileToString(final String path) throws IOException {
        try (final BufferedInputStream br = new BufferedInputStream(ClassLoader
                .getSystemClassLoader().getResourceAsStream(path))) {
            final byte[] res = new byte[br.available()];
            br.read(res);
            return new String(res);
        }
    }
	
	public static void stringToFile(final String path, String arg) throws IOException {
        try (final BufferedOutputStream br = new BufferedOutputStream(new FileOutputStream(path))) {
            br.write(arg.getBytes());
        }
    }

}

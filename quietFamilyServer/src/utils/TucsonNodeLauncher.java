package utils;

import alice.tucson.service.TucsonNodeService;

/**
 * Simple launcher for a TuCSoN Node.
 *
 * @author s.mariani@unibo.it
 */
public class TucsonNodeLauncher {

    /**
     * @param args
     *            the listening port for the TuCSoN Node service
     */
    public static void main(final String[] args) {
        /*
         * Default port is 20504.
         */
        if (args.length == 0) {
            new TucsonNodeService().install();
        } else if (args.length == 1) {
            new TucsonNodeService(Integer.parseInt(args[0])).install();
        } else {
            System.out.println("USAGE: java TucsonNodeLauncher [portno]");
        }
    }

}

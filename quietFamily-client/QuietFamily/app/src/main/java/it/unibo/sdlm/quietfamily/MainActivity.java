package it.unibo.sdlm.quietfamily;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Window;
import android.widget.TextView;

import it.unibo.sdlm.quietfamily.fragment.ControlledFragment;
import it.unibo.sdlm.quietfamily.fragment.ControllerFragment;
import it.unibo.sdlm.quietfamily.fragment.OnFragmentInteractionListener;
import it.unibo.sdlm.quietfamily.fragment.SettingsFragment;

public class MainActivity extends FragmentActivity implements OnFragmentInteractionListener {

    public static final int CONTROLLER_TAB = 0;
    public static final int CONTROLLED_TAB = 1;
    public static final int SETTINGS_TAB = 2;

    /*private final String[] TAB_TITLES = {
            getString(R.string.fragment_title_controller),
            getString(R.string.fragment_title_controlled),
            getString(R.string.fragment_title_settings)
    };*/

    private class TabsPagerAdapter extends FragmentPagerAdapter{

        public TabsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch(position){
                case CONTROLLER_TAB:
                    return ControllerFragment.newInstance();
                case CONTROLLED_TAB:
                    return ControlledFragment.newInstance();
                case SETTINGS_TAB:
                    return new SettingsFragment();
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch(position){
                case CONTROLLER_TAB:
                    return getString(R.string.fragment_title_controller);
                case CONTROLLED_TAB:
                    return getString(R.string.fragment_title_controlled);
                case SETTINGS_TAB:
                    return getString(R.string.fragment_title_settings);
                default:
                    return null;
            }
        }
    }

    private TabsPagerAdapter tabsPagerAdapter;
    private ViewPager mViewPager;
    private ActionBar actionBar;


    private MessageHandler handler;
    private NodeReceiver broadcastReceiver;

    private int port = 20504;

    private static Context c;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);

        setContentView(R.layout.activity_main);
        //context init
        c = this;
        //tabspager init
        tabsPagerAdapter = new TabsPagerAdapter(getSupportFragmentManager());
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(tabsPagerAdapter);
        mViewPager.setOnPageChangeListener(
                new ViewPager.SimpleOnPageChangeListener(){
                    @Override
                    public void onPageSelected(int position) {
                        //super.onPageSelected(position);
                        getActionBar().setSelectedNavigationItem(position);
                    }
                }
        );
        //actionbar
        actionBar = getActionBar();
        //actionBar.setHomeButtonEnabled(false);
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        // Create a tab listener that is called when the user changes tabs.
        ActionBar.TabListener tabListener = new ActionBar.TabListener() {
            @Override
            public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) {

            }

            @Override
            public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {
                // When the tab is selected, switch to the
                // corresponding page in the ViewPager.
                mViewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) {

            }
        };
        //addtabs
        for(int i=0;i<tabsPagerAdapter.getCount();i++){
            actionBar.addTab(actionBar.newTab()
                            .setText(tabsPagerAdapter.getPageTitle(i))
                            .setTabListener(tabListener)
            );
        }

        if (this.handler == null) {
            this.handler = new MessageHandler();
        }
        if (this.broadcastReceiver == null) {
            this.broadcastReceiver = new NodeReceiver();
        }
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("tucson.node.LOG");
        intentFilter.addAction("tucson.node.ERROR");
        registerReceiver(this.broadcastReceiver,//this because we are in an activity
                intentFilter);
    }

    // This unbinds the service when the activity has been killed somehow
    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStart() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("tucson.node.LOG");
        intentFilter.addAction("tucson.node.ERROR");
        registerReceiver(this.broadcastReceiver,
                intentFilter);
        super.onStart();
    }

    @Override
    public void onStop() {
        unregisterReceiver(this.broadcastReceiver);
        super.onStop();
    }

    private void postLog(final String s) {
        //TucsonNode.log(s);
        //this.logTv.append("[Tucson Node] " + s + "\n");
        Log.d("SD-PROJ", "[Tucson Node] " + s + "\n");
        //this.appendLog("[Tucson Node] " + s + "\n");
        //this.performScrollDownOnLog();
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    public static Context getContext(){
        return c;
    }

    @SuppressLint("HandlerLeak")
    private class MessageHandler extends Handler {

        @Override
        public void handleMessage(final Message msg) {
            final Bundle bundle = msg.getData();
            if (bundle.containsKey("error")) {
                //MainActivity.this.logTv.append(bundle.getString("who") + " Error: "+ bundle.getString("error") + "\n");
            } else if (bundle.containsKey("log")) {
                //MainActivity.this.logTv.append(bundle.getString("who") + " " + bundle.getString("log") + "\n");
            }
            Log.d("SDPROJ",bundle.getString("who") + " Error: "
                    + bundle.getString("error") + "\n");
        }

        public void postError(final String who, final String error) {
            final Message msg = this.obtainMessage();
            final Bundle b = new Bundle();
            b.putString("who", who);
            b.putString("error", error);
            msg.setData(b);
            this.sendMessage(msg);
        }

        public void postLog(final String who, final String l, final String port) {
            final Message msg = this.obtainMessage();
            final Bundle b = new Bundle();
            b.putString("who", who);
            b.putString("log", l);
            if (port != null)
                b.putString("port", port);
            msg.setData(b);
            this.sendMessage(msg);
        }
    }

    /*****************************************************************************************
     * BROADCAST RECEIVER AND MESSAGE HANDLER FOR LOGS
     *****************************************************************************************/
    private class NodeReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(final Context context, final Intent intent) {
            final String action = intent.getAction();
            if ("tucson.node.LOG".equalsIgnoreCase(action)) {
                MainActivity.this.handler.postLog(
                        intent.getStringExtra("who"),
                        intent.getStringExtra("msg"),
                        intent.getStringExtra("port"));
            } else if ("tucson.node.ERROR".equalsIgnoreCase(action)) {
                MainActivity.this.handler.postError(
                        intent.getStringExtra("who"),
                        intent.getStringExtra("msg"));
            }
        }
    }
}

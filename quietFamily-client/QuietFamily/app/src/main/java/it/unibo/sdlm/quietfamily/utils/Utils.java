/*
 * Copyright 1999-2014 Alma Mater Studiorum - Universita' di Bologna This file
 * is part of TuCSoNAndroid <http://tucsonandroid.apice.unibo.it>. TuCSoNAndroid
 * is free software: you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version. TuCSoNAndroid is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details. You should have received a copy of the GNU Lesser General
 * Public License along with TuCSoNAndroid. If not, see
 * <https://www.gnu.org/licenses/lgpl.html>.
 */
/**
 * This work by Danilo Pianini is licensed under a Creative Commons
 * Attribution-NonCommercial-ShareAlike 3.0 Italy License. Permissions beyond
 * the scope of this license may be available at www.danilopianini.org.
 */
package it.unibo.sdlm.quietfamily.utils;

import android.content.res.AssetManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;
import java.util.List;

/**
 * Credits go to the author below.
 *
 * @author Danilo Pianini
 * @author (contributor) ste (mailto: s.mariani@unibo.it)
 * @author (contributor) Michele Bombardi (mailto:
 *         michele.bombardi@studio.unibo.it)
 * @author (contributor) Lorenzo Forcellini Reffi (mailto: lorenzo.forcellini3@studio.unibo.it)
 * @author (contributor) Simone Norcini (mailto: simone.norcini@studio.unibo.it)
 *
 */
public final class Utils {

    /**
     *
     * @param is
     *            the input stream opened on the file to read
     * @return the String representation of the content of the read file
     * @throws IOException
     *             if the file cannot be found or access permissions do not
     *             allow reading
     */
    public static String fileToString(final InputStream is) throws IOException {
        final BufferedInputStream br = new BufferedInputStream(is);
        final byte[] res = new byte[br.available()];
        br.read(res);
        br.close();
        return new String(res);
    }

    /**
     *
     * @param path
     *            the filepath toward the file to be read
     * @return the String representation of the content of the read file
     * @throws IOException
     *             if the file cannot be found or access permissions do not
     *             allow reading
     */
    public static String fileToString(final String path) throws IOException {
        final BufferedInputStream br = new BufferedInputStream(
                new FileInputStream(path));
        final byte[] res = new byte[br.available()];
        br.read(res);
        br.close();
        return new String(res);
    }
    
    /**
    *
    * @param s
    *            the message to log
    */
   public static void log(final String s) {
       System.out.println("[Android Agent] " + s);
   }
   
   public static void post(final String who, final String what, final String s, final Handler messageHandler) {
	    final Bundle bundle = new Bundle();
	    bundle.putString("who", "[Android Agent " + who + "]: ");
	    bundle.putString(what, s);
	    final Message message = messageHandler.obtainMessage();
	    message.setData(bundle);
	    messageHandler.sendMessage(message);
	}

   public static void postError(final String who, final String s, final Handler messageHandler) {
        log(who + " " + "Error: " + s);
        post(who, "error", s, messageHandler);
    }

   /**
   *
   * @param who
   *            who sends the message to log
   * @param s
   * 			the message to log
   */
   public static void postLog(final String who, final String s, final Handler messageHandler) {
        log(who + " " + s);
        post(who, "log", s, messageHandler);
    }
   
   public static String convertStreamToString(InputStream is) throws Exception {
	    BufferedReader reader = new BufferedReader(new InputStreamReader(is));
	    StringBuilder sb = new StringBuilder();
	    String line = null;
	    while ((line = reader.readLine()) != null) {
	      sb.append(line).append("\n");
	    }
	    reader.close();
	    return sb.toString();
	}

   public static String getStringFromFile (String fileName, AssetManager am) throws Exception {
	    InputStream is = am.open(fileName);
	    String ret = convertStreamToString(is);
	    //Make sure you close all streams.
	    is.close();
	    return ret;
	}

    private Utils() {
        /*
         *
         */
    }

    public static String getIp() {
        String ipPlace = null;
        try { // To get IP address when on 3G/WIFI connection
            for (final Enumeration<NetworkInterface> en = NetworkInterface
                    .getNetworkInterfaces(); en.hasMoreElements();) {
                final NetworkInterface intf = en.nextElement();
                for (final Enumeration<InetAddress> enumIpAddr = intf
                        .getInetAddresses(); enumIpAddr.hasMoreElements();) {
                    final InetAddress inetAddress = enumIpAddr.nextElement();
                    int networkPrefixL = 0;
                    if (!inetAddress.isLoopbackAddress()) {

                        List<InterfaceAddress> addressList = NetworkInterface
                                .getByInetAddress(inetAddress)
                                .getInterfaceAddresses();
                        if (addressList != null && !addressList.isEmpty()) {
                            if(addressList.size() == 1) {
                                networkPrefixL = NetworkInterface
                                        .getByInetAddress(inetAddress)
                                        .getInterfaceAddresses().get(0)
                                        .getNetworkPrefixLength();
                            }
                            else {
                                networkPrefixL = NetworkInterface
                                        .getByInetAddress(inetAddress)
                                        .getInterfaceAddresses().get(1)
                                        .getNetworkPrefixLength();
                            }
                        }

                        ipPlace = inetAddress.getHostAddress().toString();
                                //+ "/" + networkPrefixL;
                        //return ipPlace;
                    }
                }
            }
        } catch (final SocketException e) {
            //this.postError("Unable to get ip place: " + e.getMessage());
        }
        return ipPlace;
        /*
        if (ipPlace == null) {
            return "NA";
        }
        return ipPlace;*/
    }
}

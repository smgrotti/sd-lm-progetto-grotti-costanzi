package it.unibo.sdlm.quietfamily;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import alice.logictuple.LogicTuple;
import alice.logictuple.exceptions.InvalidLogicTupleException;
import alice.tucson.api.EnhancedSynchACC;
import alice.tucson.api.ITucsonOperation;
import alice.tucson.api.NegotiationACC;
import alice.tucson.api.TucsonAgentId;
import alice.tucson.api.TucsonMetaACC;
import alice.tucson.api.TucsonTupleCentreId;
import alice.tucson.api.exceptions.TucsonInvalidAgentIdException;
import alice.tucson.api.exceptions.TucsonInvalidTupleCentreIdException;
import alice.tucson.api.exceptions.TucsonOperationNotPossibleException;
import alice.tucson.api.exceptions.UnreachableNodeException;
import alice.tuplecentre.api.exceptions.OperationTimeOutException;
import alice.tuprolog.Var;
import it.unibo.sdlm.quietfamily.utils.UtilKB;

public class ControlledService extends Service {

    private boolean run = true;

    public ControlledService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    private Context mContext;

    private ScheduledThreadPoolExecutor timerPool;

    private class ControlledPeriodicTask implements Runnable{

        @Override
        public void run() {
            final TucsonAgentId aid;
            try {
                aid = new TucsonAgentId("controlled_service_agent");
                final NegotiationACC negACC = TucsonMetaACC.getNegotiationContext(aid, UtilKB.DEFAULT_HOST,
                        Integer.valueOf(UtilKB.DEFAULT_PORT));
                final EnhancedSynchACC acc = negACC.playDefaultRole();
                //default tuple centre for client host
                final TucsonTupleCentreId controlledTc = new TucsonTupleCentreId(UtilKB.DEFAULT_TCNAME,UtilKB.DEFAULT_HOST,UtilKB.DEFAULT_PORT);

                final LogicTuple template = LogicTuple.parse("retrieveCurrentPlace(ph,"
                        + new Var("P") + ")");
                ITucsonOperation op = acc.out(controlledTc, template, null);

                //inp non blocking
                op = acc
                        .inp(controlledTc, LogicTuple.parse("currentPlace(Pos)"), null);
                LogicTuple res = null;
                if (op.isResultSuccess()) {
                    res = op.getLogicTupleResult();
                    Log.d("SD-PROJ","Current place result is "+res+ "place is "+res.getArg(0));
                    //Utils.postLog("", "current_place result is " + res, messageHandler);
                }

            } catch (TucsonInvalidAgentIdException e) {
                e.printStackTrace();
            } catch (OperationTimeOutException e) {
                e.printStackTrace();
            } catch (TucsonInvalidTupleCentreIdException e) {
                e.printStackTrace();
            } catch (UnreachableNodeException e) {
                e.printStackTrace();
            } catch (TucsonOperationNotPossibleException e) {
                e.printStackTrace();
            } catch (InvalidLogicTupleException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onCreate() {
        timerPool = new ScheduledThreadPoolExecutor(1);

        mContext = MainActivity.getContext();//context for notification
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Toast.makeText(this, "controlled service starting", Toast.LENGTH_SHORT).show();
        Log.d("SD-PROJ","ControlledService STARTED");

        timerPool.scheduleAtFixedRate(new ControlledPeriodicTask(),0,UtilKB.CONTROLLED_PERIOD, TimeUnit.SECONDS);

        // If we get killed, after returning from here, restart
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        run = false;
        Log.d("SD-PROJ","CONTROLLED SERVICE STOPPED ONDESTROY");
        timerPool.shutdown();

        Toast.makeText(this, "controlled service done", Toast.LENGTH_SHORT).show();
    }

    public boolean isRunning() {
        return this.run;
    }
}

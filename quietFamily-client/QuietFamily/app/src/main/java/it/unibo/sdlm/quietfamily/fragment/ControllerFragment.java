package it.unibo.sdlm.quietfamily.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import alice.logictuple.LogicTuple;
import alice.logictuple.exceptions.InvalidLogicTupleException;
import alice.tucson.api.EnhancedSynchACC;
import alice.tucson.api.ITucsonOperation;
import alice.tucson.api.NegotiationACC;
import alice.tucson.api.TucsonAgentId;
import alice.tucson.api.TucsonMetaACC;
import alice.tucson.api.TucsonTupleCentreId;
import alice.tucson.api.exceptions.TucsonInvalidAgentIdException;
import alice.tucson.api.exceptions.TucsonInvalidTupleCentreIdException;
import alice.tucson.api.exceptions.TucsonOperationNotPossibleException;
import alice.tucson.api.exceptions.UnreachableNodeException;
import alice.tuplecentre.api.exceptions.OperationTimeOutException;
import it.unibo.sdlm.quietfamily.ControllerService;
import it.unibo.sdlm.quietfamily.R;
import it.unibo.sdlm.quietfamily.utils.UtilKB;
import it.unibo.sdlm.quietfamily.utils.Utils;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ControllerFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ControllerFragment extends Fragment {

    private OnFragmentInteractionListener mListener;

    private TextView logTv;
    private TextView statusTv;
    private TextView buttonTv;

    private ImageButton activate_btn;

    private boolean isRegistered = false;
    private boolean isActive=false;
    private String username;

    private boolean mIsBound;
    private ControllerService mBoundService;

    public ControllerFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment ControllerFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ControllerFragment newInstance() {
        ControllerFragment fragment = new ControllerFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
        //isActive = sharedPref.getBoolean(getString(R.string.pref_controller_active), false);
        username = sharedPref.getString(getString(R.string.pref_username), "");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View layout = inflater.inflate(R.layout.fragment_controller, container, false);

        statusTv = (TextView)layout.findViewById(R.id.controller_status_tv);
        logTv = (TextView)layout.findViewById(R.id.controller_log_tv);
        buttonTv = (TextView)layout.findViewById(R.id.controller_button_tv);

        activate_btn = ((ImageButton)layout.findViewById(R.id.controller_activate_btn));

        activate_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activateController(v);
            }
        });

        return layout;
    }

    @Override
    public void onStart() {
        super.onStart();

        if(isActive){
            activate_btn.setBackgroundResource(R.drawable.red_button);
            statusTv.setText("ACTIVATED");
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void activateController(View view){
        username = getActivity().getPreferences(Context.MODE_PRIVATE).getString(getString(R.string.pref_username), "");
        if(username != ""){
            if(isActive){
                isActive=false;
                SharedPreferences sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(getString(R.string.pref_controller_active), false);
                editor.commit();

                //deregister
                stopControllerService();

                activate_btn.setBackgroundResource(R.drawable.green_button);
                statusTv.setText("DE-ACTIVATED");
                buttonTv.setText(getString(R.string.controlled_button_activate));
            } else {
                isActive = true;
                SharedPreferences sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(getString(R.string.pref_controller_active), true);
                editor.commit();
                //activate_btn.setBackgroundResource(R.drawable.red_button);
                //statusTv.setText("ACTIVATED");

                register();
            }
        } else {
            postLog("Set the username in settings fragment");
        }
    }

    private void register(){
        new AsyncTask<Void,Void,Void>(){

            @Override
            protected Void doInBackground(Void... params) {
                //controllare se è attivo il centro di tuple e se sono registrato su username
                //retrieve agent
                try {
                    final TucsonAgentId aid = new TucsonAgentId("activate_controller_agent");
                    final NegotiationACC negACC = TucsonMetaACC.getNegotiationContext(aid, UtilKB.DEFAULT_HOST,
                            Integer.valueOf(UtilKB.DEFAULT_PORT));
                    final EnhancedSynchACC acc = negACC.playDefaultRole();
                    //default tuple centre for client host
                    final TucsonTupleCentreId controllerTc = new TucsonTupleCentreId(UtilKB.DEFAULT_TCNAME,UtilKB.DEFAULT_HOST,UtilKB.DEFAULT_PORT);
                    //server tuple centre
                    final TucsonTupleCentreId serverTc = new TucsonTupleCentreId(UtilKB.SERVER_TCNAME,UtilKB.SERVER_HOST,UtilKB.SERVER_PORT);

                    String myIp = Utils.getIp();

                    //registration phase out to server tuple centre
                    final LogicTuple regTuple = LogicTuple.parse(
                            "controller("+
                                    username+","+
                                    UtilKB.DEFAULT_TCNAME+","+
                                    myIp+","+//UtilKB.CLIENT_HOST+","+
                                    UtilKB.DEFAULT_PORT+")"
                    );

                    ITucsonOperation op = acc.out(serverTc, regTuple, null);

                    if(!op.isResultSuccess()){
                        //error handling TODO
                        return null;
                    }

                    //o fai partire service
                    startControllerService();
                } catch (TucsonInvalidAgentIdException e) {
                    e.printStackTrace();
                } catch (OperationTimeOutException e) {
                    e.printStackTrace();
                } catch (UnreachableNodeException e) {
                    e.printStackTrace();
                } catch (TucsonOperationNotPossibleException e) {
                    e.printStackTrace();
                } catch (TucsonInvalidTupleCentreIdException e) {
                    e.printStackTrace();
                } catch (InvalidLogicTupleException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                Toast.makeText(getActivity(), "controller service started",
                        Toast.LENGTH_SHORT).show();
                activate_btn.setBackgroundResource(R.drawable.red_button);
                statusTv.setText("ACTIVATED");
                buttonTv.setText(getString(R.string.controlled_button_deactivate));
            }
        }.execute();
    }

    /**
     *
     * @return wether the TuCSoN Node service is running
     */
    public boolean serviceIsRunning() {
        return /*this.mBoundService != null &&*/ this.mBoundService.isRunning();
    }

    private void postLog(final String s) {
        this.logTv.append("[ControllerFragment] " + s + "\n");
    }

    private void startControllerService() {
        final Intent intent = new Intent(this.getActivity(), ControllerService.class);
        getActivity().startService(intent);
    }

    /**
     * To stop TuCSoN Node service
     */
    public void stopControllerService() {
        this.postLog("Stopping QF controller service");
        getActivity().stopService(new Intent(this.getActivity(),
                ControllerService.class));
    }

}

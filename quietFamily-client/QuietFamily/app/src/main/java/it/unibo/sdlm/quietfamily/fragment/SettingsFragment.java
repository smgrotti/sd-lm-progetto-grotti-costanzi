package it.unibo.sdlm.quietfamily.fragment;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import it.unibo.sdlm.quietfamily.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingsFragment extends Fragment/*PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener*/{

    private Button username_btn;

    public SettingsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View layout = inflater.inflate(R.layout.fragment_settings, container, false);
        username_btn = (Button)layout.findViewById(R.id.apply_uname_btn);
        username_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeUsername(v);
            }
        });
        return layout;
    }

    @Override
    public void onResume() {
        super.onResume();
        String username = getActivity().getPreferences(Context.MODE_PRIVATE).getString(getString(R.string.pref_username),"");
        if(!"".equals(username)){
            ((EditText)getActivity().findViewById(R.id.et_username)).setText(username);
        } else {
            ((EditText)getActivity().findViewById(R.id.et_username)).setText("Set your username");
        }

    }

    public void changeUsername(View view){
        String username = ((EditText)getActivity().findViewById(R.id.et_username)).getText().toString();
        SharedPreferences sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(getString(R.string.pref_username), username);
        editor.commit();
    }
    /*
    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Load the preferences from an XML resource
        this.addPreferencesFromResource(R.xml.preferences);
    }

    @Override
    public void onPause() {
        super.onPause();
        this.getPreferenceScreen().getSharedPreferences()
                .unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        this.getPreferenceScreen().getSharedPreferences()
                .registerOnSharedPreferenceChangeListener(this);
        //set summary
        this.onSharedPreferenceChanged(this.getPreferenceScreen().getSharedPreferences(),getString(R.string.pref_username));
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        String string = sharedPreferences.getString(key, "");
        if(key.equals(getString(R.string.pref_username))){
            final Preference connectionPref = this.findPreference(key);
            connectionPref.setSummary(this
                    .getString(R.string.pref_username_summ)
                    + "Current: "
                    + string);
        } else {

        }
    }*/
}

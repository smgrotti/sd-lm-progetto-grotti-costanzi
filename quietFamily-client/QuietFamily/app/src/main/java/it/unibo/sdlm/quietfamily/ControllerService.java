package it.unibo.sdlm.quietfamily;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import alice.logictuple.LogicTuple;
import alice.logictuple.exceptions.InvalidLogicTupleException;
import alice.tucson.api.EnhancedSynchACC;
import alice.tucson.api.ITucsonOperation;
import alice.tucson.api.NegotiationACC;
import alice.tucson.api.TucsonAgentId;
import alice.tucson.api.TucsonMetaACC;
import alice.tucson.api.TucsonTupleCentreId;
import alice.tucson.api.exceptions.TucsonInvalidAgentIdException;
import alice.tucson.api.exceptions.TucsonInvalidTupleCentreIdException;
import alice.tucson.api.exceptions.TucsonOperationNotPossibleException;
import alice.tucson.api.exceptions.UnreachableNodeException;
import alice.tuplecentre.api.exceptions.OperationTimeOutException;
import it.unibo.sdlm.quietfamily.utils.UtilKB;

public class ControllerService extends Service {

    private boolean run = true;

    public ControllerService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    private Context mContext;

    private Looper mServiceLooper;
    private ServiceHandler mServiceHandler;

    //TODO gestione agente crearlo all'oncreate
    // Handler that receives messages from the thread
    private final class ServiceHandler extends Handler {
        public ServiceHandler(Looper looper) {
            super(looper);
        }
        @Override
        public void handleMessage(Message msg) {
            //customop
            //fare loop di in +spara notifica
            final TucsonAgentId aid;
            try {
                aid = new TucsonAgentId("controller_service_agent");
                final NegotiationACC negACC = TucsonMetaACC.getNegotiationContext(aid, UtilKB.DEFAULT_HOST,
                        Integer.valueOf(UtilKB.DEFAULT_PORT));
                final EnhancedSynchACC acc = negACC.playDefaultRole();
                //default tuple centre for client host
                final TucsonTupleCentreId controllerTc = new TucsonTupleCentreId(UtilKB.DEFAULT_TCNAME,UtilKB.DEFAULT_HOST,UtilKB.DEFAULT_PORT);

                //in tuple
                LogicTuple dangerTuple = LogicTuple.parse(
                        "danger(outside(Child))"
                );

                int i = 0;

                Log.d("SD-PROJ","service prima di while");
                //TODO termination condition
                while(run){
                    dangerTuple = LogicTuple.parse(
                            "danger(outside(Child"+i+"))"
                    );
                    ITucsonOperation op = acc.in(controllerTc, dangerTuple, null);
                    Log.d("SD-PROJ","service dopo op while");
                    LogicTuple res = null;
                    if(op.isResultSuccess() && run){
                        res = op.getLogicTupleResult();
                        String childname = ""+res.getArg("outside").getArg(0);
                        //res.getVarValue("Child");
                        //show notification
                        showNotification(childname);
                        Log.d("SD-PROJ", "service dopo notifica");
                    }
                    i++;
                }

            } catch (TucsonInvalidAgentIdException e) {
                e.printStackTrace();
            } catch (OperationTimeOutException e) {
                e.printStackTrace();
            } catch (TucsonInvalidTupleCentreIdException e) {
                e.printStackTrace();
            } catch (UnreachableNodeException e) {
                e.printStackTrace();
            } catch (TucsonOperationNotPossibleException e) {
                e.printStackTrace();
            } catch (InvalidLogicTupleException e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public void onCreate() {
        // Start up the thread running the service.  Note that we create a
        // separate thread because the service normally runs in the process's
        // main thread, which we don't want to block.  We also make it
        // background priority so CPU-intensive work will not disrupt our UI.
        HandlerThread thread = new HandlerThread("ServiceStartArguments");//,
                //Process.THREAD_PRIORITY_BACKGROUND);
        thread.start();

        // Get the HandlerThread's Looper and use it for our Handler
        mServiceLooper = thread.getLooper();
        mServiceHandler = new ServiceHandler(mServiceLooper);

        mContext = MainActivity.getContext();//context for notification
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Toast.makeText(this, "controller service starting", Toast.LENGTH_SHORT).show();


        // For each start request, send a message to start a job and deliver the
        // start ID so we know which request we're stopping when we finish the job
        Message msg = mServiceHandler.obtainMessage();
        msg.arg1 = startId;
        mServiceHandler.sendMessage(msg);


        // If we get killed, after returning from here, restart
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        run = false;
        mServiceLooper.quit();
        Toast.makeText(this, "service done", Toast.LENGTH_SHORT).show();
    }

    /**
     * A service-related notification has been clicked, thus the service GUI has
     * to be shown
     */
    public void showNotification(String childName) {
        final CharSequence text = "Child "+childName+" is outside the comfort zone.";
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        final NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                this.mContext).setSmallIcon(R.drawable.tucson_icon)
                .setContentTitle("QuietFam ALARM").setContentText(text).setSound(alarmSound);
        final Intent resultIntent = new Intent(this.mContext,
                MainActivity.class);
        resultIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        resultIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        /*
         * Because clicking the notification opens a new ("special") activity,
         * there's no need to create an artificial back stack.
         */
        final PendingIntent resultPendingIntent = PendingIntent.getActivity(
                this.mContext, 0, resultIntent, 0);
        mBuilder.setContentIntent(resultPendingIntent);
        final Notification notification = mBuilder.build();
        final NotificationManager notificationManager = (NotificationManager) this
                .getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, notification);
    }

    public boolean isRunning() {
        return this.run;
    }
}

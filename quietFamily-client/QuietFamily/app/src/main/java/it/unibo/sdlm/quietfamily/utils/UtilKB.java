package it.unibo.sdlm.quietfamily.utils;

public class UtilKB {
    public static final String DEFAULT_HOST = "localhost";
    public static final String DEFAULT_PORT = "20504";
    public static final String DEFAULT_TCNAME = "default";
    public static final String CLIENT_HOST = "192.168.0.102";

    public static final String SERVER_HOST = "192.168.43.13";//"192.168.43.25";//"localhost";//"192.168.0.100";
    public static final String SERVER_PORT = "20504";
    public static final String SERVER_TCNAME = "coordinationTC";//"coordinationTC";

    public static final String SERVER_POS = "coords(44.431456,12.325656)";

    public static final int SERVER_RADIUS = 20;

    public static final int CONTROLLED_PERIOD = 20;//in seconds
}

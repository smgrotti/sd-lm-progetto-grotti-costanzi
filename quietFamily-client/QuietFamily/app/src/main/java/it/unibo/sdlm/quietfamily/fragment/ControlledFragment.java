package it.unibo.sdlm.quietfamily.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import alice.logictuple.LogicTuple;
import alice.logictuple.exceptions.InvalidLogicTupleException;
import alice.tucson.api.EnhancedSynchACC;
import alice.tucson.api.ITucsonOperation;
import alice.tucson.api.NegotiationACC;
import alice.tucson.api.TucsonAgentId;
import alice.tucson.api.TucsonMetaACC;
import alice.tucson.api.TucsonTupleCentreId;
import alice.tucson.api.exceptions.TucsonInvalidAgentIdException;
import alice.tucson.api.exceptions.TucsonInvalidTupleCentreIdException;
import alice.tucson.api.exceptions.TucsonOperationNotPossibleException;
import alice.tucson.api.exceptions.UnreachableNodeException;
import alice.tuplecentre.api.exceptions.OperationTimeOutException;
import it.unibo.sdlm.quietfamily.ControlledService;
import it.unibo.sdlm.quietfamily.R;
import it.unibo.sdlm.quietfamily.utils.UtilKB;
import it.unibo.sdlm.quietfamily.utils.Utils;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ControlledFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ControlledFragment extends Fragment {

    private OnFragmentInteractionListener mListener;

    private TextView logTv;
    private TextView statusTv;
    private TextView buttonTv;

    private ImageButton activate_btn;

    private boolean isActive=false;
    private String username;
    private LogicTuple event;
    private LogicTuple guards;
    private LogicTuple body;

    private MessageHandler messageHandler;


    public ControlledFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment ControlledFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ControlledFragment newInstance() {
        ControlledFragment fragment = new ControlledFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
        //isActive = sharedPref.getBoolean(getString(R.string.pref_controlled_active), false);

        username = sharedPref.getString(getString(R.string.pref_username), "");

        this.messageHandler = new MessageHandler();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View layout = inflater.inflate(R.layout.fragment_controlled, container, false);

        statusTv = (TextView)layout.findViewById(R.id.controlled_status_tv);
        logTv = (TextView)layout.findViewById(R.id.controlled_log_tv);
        buttonTv = (TextView)layout.findViewById(R.id.controlled_button_tv);

        activate_btn = ((ImageButton)layout.findViewById(R.id.controlled_activate_btn));

        activate_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activateControlled(v);
            }
        });

        return layout;
    }

    @Override
    public void onStart() {
        super.onStart();

        if(isActive){
            activate_btn.setBackgroundResource(R.drawable.red_button);
            statusTv.setText("ACTIVATED");
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void activateControlled(View view){

        if(isActive){
            Toast.makeText(getActivity(),"Stopping controlled service",Toast.LENGTH_SHORT).show();
            SharedPreferences sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putBoolean(getString(R.string.pref_controlled_active), false);
            editor.commit();
            isActive=false;

            deregister();

            //activate_btn.setBackgroundResource(R.drawable.green_button);
            //statusTv.setText("DE-ACTIVATED");
        } else {
            Toast.makeText(getActivity(),"Starting controlled service",Toast.LENGTH_SHORT).show();
            SharedPreferences sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putBoolean(getString(R.string.pref_controlled_active), true);
            editor.commit();
            isActive = true;

            register();

            //activate_btn.setBackgroundResource(R.drawable.red_button);
            //statusTv.setText("ACTIVATED");
        }
    }

    private void register(){
        Log.d("SD-PROJ", "start register");
        new RegisterTask().execute();
        Log.d("SD-PROJ", "end register");
    }

    private void deregister(){
        Log.d("SD-PROJ", "start deregister");
        new DeregisterTask().execute();
        Log.d("SD-PROJ", "end deregister");
    }

    private void postLog(final String s) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                logTv.append("[ControlledFragment] " + s + "\n");
            }
        });
    }

    private void startControlledService() {
        this.postLog("Starting QF controlled service");
        final Intent intent = new Intent(this.getActivity(), ControlledService.class);
        getActivity().startService(intent);
    }

    /**
     * To stop TuCSoN Node service
     */
    public void stopControlledService() {
        this.postLog("Stopping QF controlled service");
        getActivity().stopService(new Intent(this.getActivity(),
                ControlledService.class));
    }

    private class RegisterTask extends AsyncTask<Void, Void, Void>{

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            activate_btn.setBackgroundResource(R.drawable.red_button);
            statusTv.setText("ACTIVATED");
            buttonTv.setText(getString(R.string.controlled_button_deactivate));
        }

        @Override
        protected Void doInBackground(Void... params) {
            //controllare se è attivo il centro di tuple e il servizio di geoloc e se sono registrato su username
            //retrieve agent
            Log.d("SD-PROJ", "start register task doinbackground");
            try {
                final TucsonAgentId aid = new TucsonAgentId("activate_controlled_agent");
                final NegotiationACC negACC = TucsonMetaACC.getNegotiationContext(aid, UtilKB.DEFAULT_HOST,
                        Integer.valueOf(UtilKB.DEFAULT_PORT));
                final EnhancedSynchACC acc = negACC.playDefaultRole();
                //default tuple centre for client host
                final TucsonTupleCentreId controlledTc = new TucsonTupleCentreId(UtilKB.DEFAULT_TCNAME,UtilKB.DEFAULT_HOST,UtilKB.DEFAULT_PORT);
                //server tuple centre
                final TucsonTupleCentreId serverTc = new TucsonTupleCentreId(UtilKB.SERVER_TCNAME,UtilKB.SERVER_HOST,UtilKB.SERVER_PORT);

                String myIp = Utils.getIp();

                //registration phase out to server tuple centre
                final LogicTuple regTuple = LogicTuple.parse(
                        "controlled("+
                                username+","+
                                UtilKB.DEFAULT_TCNAME+","+
                                myIp+","+//UtilKB.CLIENT_HOST+","+
                                UtilKB.DEFAULT_PORT+")"
                );

                ITucsonOperation op = acc.out(serverTc, regTuple, null);

                if(!op.isResultSuccess()){
                    //error handling TODO
                    return null;
                }

                //TEST4 with nop to notify only one time

                final LogicTuple eventRetrieve = LogicTuple.parse("out(retrieveCurrentPlace(S,P))");
                final LogicTuple guardsRetrieve = LogicTuple.parse("( completion, operation )");
                final LogicTuple bodyRetrieve = LogicTuple.parse("( " + "current_place(S,P),"
                        + "out(currentPlace(P)) )");

                acc.outS(controlledTc, eventRetrieve, guardsRetrieve, bodyRetrieve, null);

                Log.e("SD-PROJ", "REGISTER TASK AFTER Retrieve reaction");

                //current place not near
                final LogicTuple eventCPlacenotNear = LogicTuple.parse("out(currentPlace(P))");
                final LogicTuple guardsCPlacenotNear = LogicTuple.parse("( internal, not(near(ph,"+UtilKB.SERVER_POS+","+UtilKB.SERVER_RADIUS+")))");
                final LogicTuple bodyCPlacenotNear = LogicTuple.parse("( "+
                        "no(notified(O)),"+
                        UtilKB.SERVER_TCNAME+"@"+UtilKB.SERVER_HOST+":"+UtilKB.SERVER_PORT +" ? "+
                        "out(outside("+username+")),"+
                        "out(notified(out))"+
                        " )");

                acc.outS(controlledTc, eventCPlacenotNear, guardsCPlacenotNear, bodyCPlacenotNear, null);

                Log.e("SD-PROJ", "REGISTER TASK AFTER current place not near reaction");

                //current place near
                final LogicTuple eventCPlaceNear = LogicTuple.parse("out(currentPlace(P))");
                final LogicTuple guardsCPlaceNear = LogicTuple.parse("( internal, near(ph,"+UtilKB.SERVER_POS+","+UtilKB.SERVER_RADIUS+") )");
                final LogicTuple bodyCPlaceNear = LogicTuple.parse("( "+
                        "inp(notified(out))"+
                        " )");

                acc.outS(controlledTc, eventCPlaceNear, guardsCPlaceNear, bodyCPlaceNear, null);

                Log.e("SD-PROJ", "REGISTER TASK AFTER current place near reaction");

                //current place every time
                final LogicTuple eventCPlace = LogicTuple.parse("out(currentPlace(P))");
                final LogicTuple guardsCPlace = LogicTuple.parse("( internal, completion )");
                final LogicTuple bodyCPlace = LogicTuple.parse("( "+
                        "in(retrieveCurrentPlace(S,X))"+
                        " )");

                acc.outS(controlledTc, eventCPlace, guardsCPlace, bodyCPlace, null);

                Log.e("SD-PROJ", "REGISTER TASK AFTER current place everytime reaction");

                startControlledService();

                Log.d("SD-PROJ", "ControlledService STARTED from FRAGMENT");
            } catch (TucsonInvalidAgentIdException e) {
                e.printStackTrace();
            } catch (OperationTimeOutException e) {
                e.printStackTrace();
            } catch (UnreachableNodeException e) {
                e.printStackTrace();
            } catch (TucsonOperationNotPossibleException e) {
                e.printStackTrace();
            } catch (TucsonInvalidTupleCentreIdException e) {
                e.printStackTrace();
            } catch (InvalidLogicTupleException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    private class DeregisterTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            activate_btn.setBackgroundResource(R.drawable.green_button);
            statusTv.setText("DE-ACTIVATED");
            buttonTv.setText(getString(R.string.controlled_button_activate));
        }

        @Override
        protected Void doInBackground(Void... params) {
            Log.d("SD-PROJ", "start deregister task doinbackground");
            try {
                final TucsonAgentId aid = new TucsonAgentId("deactivate_controlled_agent");
                final NegotiationACC negACC = TucsonMetaACC.getNegotiationContext(aid, UtilKB.DEFAULT_HOST,
                        Integer.valueOf(UtilKB.DEFAULT_PORT));
                final EnhancedSynchACC acc = negACC.playDefaultRole();
                //default tuple centre for client host

                final TucsonTupleCentreId controlledTc = new TucsonTupleCentreId(UtilKB.DEFAULT_TCNAME,UtilKB.DEFAULT_HOST,UtilKB.DEFAULT_PORT);

                final LogicTuple eventRetrieve = LogicTuple.parse("out(retrieveCurrentPlace(S,P))");
                final LogicTuple guardsRetrieve = LogicTuple.parse("( completion, operation )");
                final LogicTuple bodyRetrieve = LogicTuple.parse("( " + "current_place(S,P),"
                        + "out(currentPlace(P)) )");

                acc.inS(controlledTc, eventRetrieve, guardsRetrieve, bodyRetrieve, null);

                Log.e("SD-PROJ", "DEREGISTER TASK AFTER Retrieve reaction");

                //current place not near
                final LogicTuple eventCPlacenotNear = LogicTuple.parse("out(currentPlace(P))");
                final LogicTuple guardsCPlacenotNear = LogicTuple.parse("( internal, not(near(ph,"+UtilKB.SERVER_POS+","+UtilKB.SERVER_RADIUS+")))");
                final LogicTuple bodyCPlacenotNear = LogicTuple.parse("( "+
                        "no(notified(O)),"+
                        UtilKB.SERVER_TCNAME+"@"+UtilKB.SERVER_HOST+":"+UtilKB.SERVER_PORT +" ? "+
                        "out(outside("+username+")),"+
                        "out(notified(out))"+
                        " )");

                acc.inS(controlledTc, eventCPlacenotNear, guardsCPlacenotNear, bodyCPlacenotNear, null);

                Log.e("SD-PROJ", "DEREGISTER TASK AFTER current place not near reaction");

                //current place near
                final LogicTuple eventCPlaceNear = LogicTuple.parse("out(currentPlace(P))");
                final LogicTuple guardsCPlaceNear = LogicTuple.parse("( internal, near(ph,"+UtilKB.SERVER_POS+","+UtilKB.SERVER_RADIUS+") )");
                final LogicTuple bodyCPlaceNear = LogicTuple.parse("( "+
                        "inp(notified(out))"+
                        " )");

                acc.inS(controlledTc, eventCPlaceNear, guardsCPlaceNear, bodyCPlaceNear, null);

                Log.e("SD-PROJ", "DEREGISTER TASK AFTER current place near reaction");

                //current place every time
                final LogicTuple eventCPlace = LogicTuple.parse("out(currentPlace(P))");
                final LogicTuple guardsCPlace = LogicTuple.parse("( internal, completion )");
                final LogicTuple bodyCPlace = LogicTuple.parse("( "+
                        "in(retrieveCurrentPlace(S,X))"+
                        " )");

                acc.inS(controlledTc, eventCPlace, guardsCPlace, bodyCPlace, null);

                Log.e("SD-PROJ", "DEREGISTER TASK AFTER current place everytime reaction");

                stopControlledService();

                Log.d("SD-PROJ", "ControlledService STOPPED");
            } catch (UnreachableNodeException e) {
                e.printStackTrace();
            } catch (TucsonOperationNotPossibleException e) {
                e.printStackTrace();
            } catch (TucsonInvalidTupleCentreIdException e) {
                e.printStackTrace();
            } catch (TucsonInvalidAgentIdException e) {
                e.printStackTrace();
            } catch (OperationTimeOutException e) {
                e.printStackTrace();
            } catch (InvalidLogicTupleException e) {
                e.printStackTrace();
            }

            return null;
        }
    }

    @SuppressLint("HandlerLeak")
    private class MessageHandler extends Handler {

        @Override
        public void handleMessage(final Message msg) {
            final Bundle bundle = msg.getData();
            if (bundle.containsKey("error")) {
                ControlledFragment.this.logTv.append(bundle.getString("who") + " Error: "
                        + bundle.getString("error") + "\n");
            } else if (bundle.containsKey("log")) {
                ControlledFragment.this.logTv.append(bundle.getString("who") + " "
                        + bundle.getString("log") + "\n");
            }
        }
    }
}
